package prova_oop;

public class Book {
	String author;
	String edition;
	String volumes;
	
	public Book(String author, String edition, String volumes) {
		super();
		this.author = author;
		this.edition = edition;
		this.volumes = volumes;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getEdition() {
		return edition;
	}
	public void setEdition(String edition) {
		this.edition = edition;
	}
	public String getVolume() {
		return volumes;
	}
	public void setVolume(String volume) {
		this.volumes = volumes;
	}
	

}
